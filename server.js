var express = require('express');
var bodyParser = require('body-parser');
var port = +process.argv[2];

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var authorId = 1;
var bookId = 1;
var books = [];
var authors = [];

app.all('/*', function (req, res, next){
	console.log(req.method + " " + req.url);
	next();
});

// GET  pobranie kolekcji books
app.get('/books', function (req, res) {
    console.log('Odczyt listy ksi¹¿ek');
    res.json(books);
});

// GET  pobranie kolekcji authors
app.get('/authors', function (req, res) {
    console.log('Odczyt listy autorów');
    res.json(authors);
});

// GET/id  pobranie elementu kolekcji books
app.get('/books/:id', function (req, res) {
    console.log('Odczyt ksi¹¿ki o id = ' + req.params.id);
    var book = findBook(parseInt(req.params.id, 10));
    if (book === null) {
        res.send(404);
    }
    else {
        res.json(book);
    }
});

function findBook(id) {
    for (var i = 0; i < books.length; i++) {
        if (books[i].id === id) {
            return books[i];
        }
    }
    return null;
}

// GET/id  pobranie elementu kolekcji authors
app.get('/authors/:id', function (req, res) {
    console.log('Odczyt autora o id = ' + req.params.id);
    var author = findAuthor(parseInt(req.params.id, 10));
    if (author === null) {
        res.send(404);
    }
    else {
        res.json(author);
    }
});

function findAuthor(id) {
    for (var i = 0; i < authors.length; i++) {
        if (authors[i].id === id) {
            return authors[i];
        }
    }
    return null;
}



// POST  dodanie elementu do kolekcji books
app.post('/books', function (req, res) {
    var book = req.body;
    book.id = bookId++;
    books.push(book);
    console.log('Zapisywanie osoby: ' + JSON.stringify(book));
    res.send(book);
});

// POST  dodanie elementu do kolekcji authors
app.post('/authors', function (req, res) {
    var author = req.body;
    author.id = authorId++;
    authors.push(author);
    console.log('Zapisywanie osoby: ' + JSON.stringify(author));
    res.send(author);
});

// PUT/id  edycja elementu kolekcji books
app.put('/books/:id', function (req, res) {
    var book = req.body;
    console.log('Edycja danych ksi¹¿ki: ' + JSON.stringify(book));
    var currentBook = findBook(parseInt(req.params.id, 10));
    if (currentBook === null) {
        res.send(404);
    }
    else {
        currentBook.title = book.title;
        currentBook.author = book.author;
        currentBook.date = book.date;
        res.send(book);
    }
});


// PUT/id  edycja elementu kolekcji author
app.put('/authors/:id', function (req, res) {
    var author = req.body;
    console.log('Edycja danych autora: ' + JSON.stringify(author));
    var currentAuthor = findAuthor(parseInt(req.params.id, 10));
    if (currentAuthor === null) {
        res.send(404);
    }
    else {
        currentAuthor.firstName = author.firstName;
        currentAuthor.lastName = author.lastName;
        res.send(author);
    }
});



// DELETE/id  usuniêcie elementu kolekcji books
app.delete('/books/:id', function (req, res) {
    console.log('Usuwanie ksi¹¿ki ' + req.params.id);
    var book = findBook(parseInt(req.params.id, 10));
    if (book === null) {
        res.send(404);
    }
    else {
        removeBook(parseInt(req.params.id, 10));
        res.send(book);
    }
});


function removeBook(id) {
    var bookIndex = 0;
    for (var i = 0; i < books.length; i++) {
        if (books[i].id === id) {
            bookIndex = i;
        }
    }
    books.splice(bookIndex, 1);
}


// DELETE/id  usuniêcie elementu kolekcji authors i jego ksi¹¿ek
app.delete('/authors/:id', function (req, res) {
    console.log('Usuwanie autora ' + req.params.id);
    var author = findAuthor(parseInt(req.params.id, 10));
    if (author === null) {
        res.send(404);
    }
    else {
        removeAuthor(parseInt(req.params.id, 10));
        res.send(author);
    }
});

function removeAuthor(id) {
    //jego ksi¹¿ki
    var bookIndex = -1;
    while (bookIndex != 0) {
        bookIndex = 0;
        for (var i = 0; i < books.length; i++) {
            if (books[i].author === id) {
                bookIndex = i;
            }
        }
        if (bookIndex != 0) {
            books.splice(bookIndex, 1);
        }
    }

    var authorIndex = 0;
    for (var i = 0; i < authors.length; i++) {
        if (authors[i].id === id) {
            authorIndex = i;
        }
    }
    authors.splice(authorIndex, 1);
}


// uruchomienie serwera
app.listen(port, function () {
    console.log("Serwer uruchomiony: localhost:" + port);
});
