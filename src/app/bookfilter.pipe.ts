import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'BookFilterPipe'})
export class BookFilterPipe implements PipeTransform {
    transform(items: any[], filter: string): any[] {
        if( !items || !filter){
            return items;
        }

        filter = filter.toLowerCase();
        var title = items.filter(item => item.title.toLowerCase().indexOf(filter) !== -1);
        var year = items.filter(item => item.year === parseInt(filter));
        var authorFirstName = items.filter(item => item.author.firstName.toLowerCase().indexOf(filter) !== -1);
        var authorLastName = items.filter(item => item.author.lastName.toLowerCase().indexOf(filter) !== -1);

        var notuniqe = title.concat(year);
        notuniqe = notuniqe.concat(authorFirstName);
        notuniqe = notuniqe.concat(authorLastName);
        console.log("notunique = " + notuniqe);
        var set = new Set(notuniqe);
        console.log("set = " + set);
        var result = Array.from(set);

        console.log(result);

        return result;
    }
}