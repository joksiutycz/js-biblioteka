import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { Author, Book } from '../interfaces'; 

const serverAddress = "http://localhost:9000";

@Injectable()
export class DataService {

  constructor(private http:HttpClient) {

   }

   getAuthors():Observable<Author[]> {
     return this.http.get<Author[]>(serverAddress + '/authors');
   }

   deleteAuthor(id){
     return this.http.delete(serverAddress + '/authors/' + id);
   }

   addAuthor(author):Observable<Author>{
     return this.http.post<Author>(serverAddress + '/authors', author);
   }

   updateAuthor(id, author):Observable<Author>{
     return this.http.put<Author>(serverAddress + '/authors/' + id, author);
   }


   getBooks():Observable<Book[]> {
    return this.http.get<Book[]>(serverAddress + '/books');
   }

   deleteBook(id){
    return this.http.delete(serverAddress + '/books/' + id);
   }

   addBook(book):Observable<Book>{
    return this.http.post<Book>(serverAddress + '/books', book);
   }

   updateBook(id, book):Observable<Book>{
    return this.http.put<Book>(serverAddress + '/books/' + id, book);
   }

}
