import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'FilterPipe'})
export class FilterPipe implements PipeTransform {
    transform(items: any[], filter: string): any[] {
        if( !items || filter.length < 3){
            return items;
        }

        filter = filter.toLowerCase();
        var firstName = items.filter(item => item.firstName.toLowerCase().indexOf(filter) !== -1);
        var lastName = items.filter(item => item.lastName.toLowerCase().indexOf(filter) !== -1);
        var notuniqe = firstName.concat(lastName);
        var set = new Set(notuniqe);
        var result = Array.from(set);

        return result;
    }
}