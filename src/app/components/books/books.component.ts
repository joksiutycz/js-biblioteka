import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

import { Author, Book } from '../../interfaces';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})


export class BooksComponent implements OnInit {

  constructor(private mydata:DataService) { }

  books:Book[];
  book:Book;
  
  authors:Author[];
  author: Author;

  authorIndex: number;

  addBookVisible: boolean = false;
  editBookVisible: boolean = false;

  editedBookIndex: number;
  editedBookId: number;

  searchText: string;

  reverse: boolean = false;
  column: string = 'title';

  ngOnInit() {

    this.books = [];
    this.authors = [];

    this.getAuthors();
    this.getBooks();
    
    this.reset();
  }

  reset(){
    this.book = {
      id: null,
      title: '',
      year: null,
      author: null
    }

    this.author = {
      id: null,
      firstName: '',
      lastName: ''
    }
  }

  changed(){
    console.log(this.author.firstName);
    this.book.author = this.author;
  }

  showAddBook(){
    this.hideEditBook();
    this.reset();
    this.addBookVisible = true;
  }
  hideAddBook(){
    this.addBookVisible = false;
  }

  showEditBook(){
    this.hideAddBook();
    this.editBookVisible = true;
  }
  hideEditBook(){
    this.editBookVisible = false;
  }

  getBooks(){
    this.mydata.getBooks()
    .subscribe(data => this.books = data);
  }

  getAuthors(){
    this.mydata.getAuthors()
    .subscribe(data => this.authors = data);
  }

  addBook(){
    this.book.author = this.author;
    this.mydata.addBook(this.book)
    .subscribe(data => this.books.push(data));

    this.reset();
  }

  editBook(){
    this.mydata.updateBook(this.editedBookId, this.book)
    .subscribe(data => this.getBooks());

    this.editBookVisible = false;

    this.reset();
  }

  deleteBook(i){
    this.mydata.deleteBook(i)
    .subscribe(data => this.getBooks());
  }

  updateBook(index){  
      this.editBookVisible = true;
      this.editedBookIndex = index;
      this.book = Object.assign ({}, this.books[this.editedBookIndex]);
      this.author = this.book.author;
      this.editedBookId = this.book.id;
      this.showEditBook();
    }   

  sortBy(column, reverse){
    this.column = column;
    this.reverse = reverse;
  }
}