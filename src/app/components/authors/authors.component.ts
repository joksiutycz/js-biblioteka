import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

import { FilterPipe } from "../../filter.pipe";

import { Author } from '../../interfaces';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  constructor(private mydata:DataService) { }

  authors:Author[];
  author:Author;

  addAuthorVisible: boolean = false;
  editAuthorVisible: boolean = false;

  editedAuthorIndex: number;
  editedAuthorId: number;

  searchText: string;
  reverse: boolean = false;
  column: string = 'firstName';

  ngOnInit() {

    this.authors = [];
    this.getAuthors();

    this.reset();
  }

  search(){

  }

  reset(){
    this.author = {
      id: null,
      firstName: '',
      lastName: ''
    }
  }

  showAddAuthor(){
    this.hideEditAuthor();
    this.reset();
    this.addAuthorVisible = true;
  }
  hideAddAuthor(){
    this.addAuthorVisible = false;
  }

  showEditAuthor(){
    this.hideAddAuthor();
    this.editAuthorVisible = true;
  }
  hideEditAuthor(){
    this.editAuthorVisible = false;
  }


  getAuthors(){
    this.mydata.getAuthors()
    .subscribe(data => this.authors = data)
  }

  addAuthor(){
    this.mydata.addAuthor(this.author)
    .subscribe(data => this.authors.push(data));

    this.reset();
    
  }

  editAuthor(){
    this.mydata.updateAuthor(this.editedAuthorId, this.author)
    .subscribe(data => this.authors[this.editedAuthorIndex] = data);

    this.editAuthorVisible = false;

    this.reset();
  }

  deleteAuthor(i){
    this.mydata.deleteAuthor(i)
    .subscribe(data => this.getAuthors());
  }

  updateAuthor(index){
      this.showEditAuthor();
      this.editAuthorVisible = true;
      this.editedAuthorIndex = index;
      this.author = Object.assign ({}, this.authors[this.editedAuthorIndex]);
      this.editedAuthorId = this.author.id;
      console.log("index = " + this.editedAuthorIndex);
      console.log("id = " + this.editedAuthorId);
    }

  sortBy(column, reverse){
    this.column = column;
    this.reverse = reverse;
  }
}
