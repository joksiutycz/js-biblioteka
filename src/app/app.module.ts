import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FilterPipe } from './filter.pipe';
import { BookFilterPipe } from './bookfilter.pipe';
import { OrderModule } from 'ngx-order-pipe';


import { AppComponent } from './app.component';
import { BooksComponent } from './components/books/books.component';
import { AuthorsComponent } from './components/authors/authors.component';

import { DataService } from './services/data.service';

const appRoutes = [
  { path: '', component:BooksComponent },
  { path: 'authors', component:AuthorsComponent }
]


@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    AuthorsComponent,
    FilterPipe,
    BookFilterPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    OrderModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
